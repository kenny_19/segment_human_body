import numpy
import cv2
import numpy as np

from Filtrage import Filtrage

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

class SegementCouleur:
    def __init__(self, name):
        self.name = name

    #segment image avec couleur
    def SegColor(self,pathF):
        #tester si path ou img
        if type(pathF) == type("a"):
            img = cv2.imread(pathF)
        else:
            img = pathF
        # 双边滤波器，同时考虑像素的空间位置和邻域的像素灰度相似性，在滤波的同时较好的保留图片的边缘
        blur4 = cv2.bilateralFilter(img, 9, 75, 75)
        #将图像与RGB颜色空间转到HSV颜色空间
        #HSV空间，H、S和V相对独立，可以准确描述像素的色度，饱和度和亮度
        hsv = cv2.cvtColor(blur4, cv2.COLOR_BGR2HSV)
        #确定像素的颜色阈值
        #利用颜色选择工具，确定需要提取的对象的像素值，这是一个繁琐的过程，需要不断尝试，以获取准确的颜色阈值区间。
        # 颜色阈值分割
        #choisir un couleur pour segment
        #couleur de masque
        low_blue = numpy.array([55, 0, 0])
        high_blue = numpy.array([118, 255, 255])
        #couluer de soin
        low_skin = numpy.array([0, 48, 50])
        high_skin = numpy.array([9, 255, 255])
        #couleur de cheveux et mantaux
        low_black = numpy.array([0, 0, 0])
        high_black = numpy.array([180, 255, 46])
        #couleur de pantalon et mantaux
        low_grey = numpy.array([0, 0, 46])
        high_grey = numpy.array([180, 43, 220])
        #couleur brun
        low_brown = numpy.array([10,100,20])
        hight_brown = numpy.array([20,255,200])

        low_val = low_skin
        high_val = high_skin


        mask = cv2.inRange(hsv, low_val, high_val)
        #cv2.imshow('mask', mask)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
        #得到最终结果
        #利用mask将图像中所有不在描述范围的像素覆盖，得到需要的对象。
        res = cv2.bitwise_and(img, img, mask=mask)
        #cv2.imshow('res', res)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
        return res

    #realser image suback mais garder les couleur
    def showImgAfterSubBack(self,img, imgBack):
        # obtient un img apre sub back
        imgSubBack = self.obtientImgApresSubBack(img, imgBack)
        img = cv2.imread(img)
        # trnsfer img
        imgRGB = cv2.cvtColor(imgSubBack, cv2.COLOR_GRAY2RGB)
        img_mask = imgRGB
        sub_img = cv2.bitwise_and(img, img_mask)
        #cv2.imshow("subBack", sub_img)
        #cv2.waitKey(0)
        return sub_img

    #sub img avec resultqt en gris
    def obtientImgApresSubBack(self,path, pthBack):
        # load the background and foreground images
        # 导入背景，前景文件
        bg = cv2.imread(pthBack)
        fg = cv2.imread(path)

        # Filtrage
        fl = Filtrage('f')
        bg = fl.filtageGauss(pthBack)
        fg = fl.filtageGauss(path)

        # convert the background and foreground images to grayscale
        # 灰度化处理
        bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
        # perform background subtraction by subtracting the foreground from
        # the background and then taking the absolute value
        # 背景减法
        sub = bgGray.astype("int32") - fgGray.astype("int32")
        sub = np.absolute(sub).astype("uint8")
        # threshold the image to find regions of the subtracted image with
        # larger pixel differences
        thresh = cv2.threshold(sub, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        # monterer img apres traite
        # cv2.imshow("subBack", thresh)
        # cv2.waitKey(0)

        #dialotion img
        thresh = self.dialotion(thresh)
        return thresh

    '''
    tester type de couleur avec HSV
    1 bleu, 2 soin, 3 noir, 4 gris, -1 inconnu
    '''
    def tsetTypeColor(self,pixel):
        if (len(pixel) != 3):
            return -1
        if pixel[0] >= 55 and pixel[0] <= 118 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[
            2] <= 255:
            return 1
        if pixel[0] >= 0 and pixel[0] <= 9 and pixel[1] >= 48 and pixel[1] <= 255 and pixel[2] >= 50 and pixel[
            2] <= 255:
            return 2
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[2] <= 46:
            return 3
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 43 and pixel[2] >= 46 and pixel[
            2] <= 220:
            return 4
        return -1

    # dialotion the img
    def dialotion(self, img):
        # 创建 核 creer coeur
        kernel = np.ones((5, 5), np.uint8)
        # 膨胀 nombre iteration=2
        dilate_img = cv2.dilate(img, kernel, 2)
        return dilate_img

    # realiser algo regionGrow avec img sub back
    def regionGrowWithSubBack(self,img, imgBack, seeds, p=1):
        # obtient un img apre sub back
        imgSubBack = self.obtientImgApresSubBack(img, imgBack)

        # charger front
        fg = cv2.imread(img)
        # transfer frond page to hsv
        hsv = cv2.cvtColor(fg, cv2.COLOR_BGR2HSV)

        img = cv2.imread(img)

        # cv2.imshow("apres sub", img)
        # img.shape. It returns a tuple of the number of rows, columns, and channels
        # height : ligne weight : cologne color:len pour list coleur
        height, weight, color = img.shape
        # Return a new array of given shape and type, filled with zeros.
        # ini carte marque pour chaque pixel comme 0
        seedMark = np.zeros(imgSubBack.shape)

        # ini carte affichier pour resultat
        resultat = np.zeros(fg.shape)

        seedList = []
        for seed in seeds:
            # list. append (x). Ajoute un élément à la fin de la liste
            seedList.append(seed)
        label = 1
        # choisir type de connect au 8 dimention
        connects = self.selectConnects(p)
        # tester si tous les seed au choix a deja utilise
        while (len(seedList) > 0):
            # tenir premier seed qu choix
            currentPoint = seedList.pop(0)
            # marquer ce seed au choix
            seedMark[currentPoint.x, currentPoint.y] = label
            # fiare un boucle avec 8 fois iteration
            for i in range(8):
                # choisir un point connecter ce point au choix
                tmpX = currentPoint.x + connects[i].x
                tmpY = currentPoint.y + connects[i].y
                # si ordre de graphe on quitter cette fois
                if tmpX < 0 or tmpY < 0 or tmpX >= height or tmpY >= weight:
                    continue

                # tester si ce pixel est correspend dans contour homme avec imgSubBack
                # on le marque et ajouter dans seed list
                if imgSubBack[tmpX, tmpY] == 255 and seedMark[tmpX, tmpY] == 0:
                    seedMark[tmpX, tmpY] = label
                    seedList.append(Point(tmpX, tmpY))
                    # detecter bleu on donne rouge
                    if self.tsetTypeColor(hsv[tmpX, tmpY]) == 1:
                        resultat[tmpX, tmpY] = np.array([255, 0, 0])
                    # detecter soin on donne vert
                    if self.tsetTypeColor(hsv[tmpX, tmpY]) == 2:
                        resultat[tmpX, tmpY] = np.array([0, 255, 0])
                    # detecter noir on donne bleu
                    if self.tsetTypeColor(hsv[tmpX, tmpY]) == 3:
                        resultat[tmpX, tmpY] = np.array([0, 0, 255])
                    # detecter gris on donne coleur jeun
                    if self.tsetTypeColor(hsv[tmpX, tmpY]) == 4:
                        resultat[tmpX, tmpY] = np.array([0, 255, 255])

        return resultat

    '''
    tester type de couleur avec HSV
    1 bleu, 2 soin, 3 noir, 4 gris, -1 inconnu
    '''

    def tsetTypeColor(self,pixel):
        if (len(pixel) != 3):
            return -1
        if pixel[0] >= 55 and pixel[0] <= 118 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[
            2] <= 255:
            return 1
        if pixel[0] >= 0 and pixel[0] <= 9 and pixel[1] >= 48 and pixel[1] <= 255 and pixel[2] >= 50 and pixel[
            2] <= 255:
            return 2
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[2] <= 46:
            return 3
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 43 and pixel[2] >= 46 and pixel[
            2] <= 220:
            return 4
        return -1

    # creer un liste avec 8 point ou 4 point qui montrer les dimentions
    def selectConnects(self,p):
        if p != 0:
            connects = [Point(-1, -1), Point(0, -1), Point(1, -1), Point(1, 0), Point(1, 1),
                        Point(0, 1), Point(-1, 1), Point(-1, 0)]
        else:
            connects = [Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0)]
        return connects

    # print img en array
    def printEnArray(self,img):
        img_array = np.asarray(img)
        print("array img：", img_array)
        print("shape of arry img：", img_array.shape)

pathName = 'media/images/homme1.png'
pathBackName = 'media/images/back.png'

sc = SegementCouleur('sc')
subimg = sc.showImgAfterSubBack(pathName,pathBackName)
#resSegCol = sc.SegColor('media/images/homme2.png')
resSegCol = sc.SegColor(subimg)

#tester find contour
img = resSegCol
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,binary = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
draw_img3 = cv2.drawContours(img.copy(), contours, -1, (0, 0, 255), 3)
cv2.imshow('show after region growing', draw_img3)
cv2.waitKey(0)
cv2.destroyAllWindows()
