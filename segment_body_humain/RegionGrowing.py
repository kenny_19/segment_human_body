import numpy
import numpy as np
import cv2
#import VideosTraiter
from matplotlib import pyplot as plt

from Filtrage import Filtrage

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y


def getGrayDiff(img, currentPoint, tmpPoint):
    return abs(int(img[currentPoint.x, currentPoint.y]) - int(img[tmpPoint.x, tmpPoint.y]))

#creer un liste avec 8 point ou 4 point qui montrer les dimentions
def selectConnects(p):
    if p != 0:
        connects = [Point(-1, -1), Point(0, -1), Point(1, -1), Point(1, 0), Point(1, 1),
                    Point(0, 1), Point(-1, 1), Point(-1, 0)]
    else:
        connects = [Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0)]
    return connects

#print img en array
def printEnArray(img):
    img_array = np.asarray(img)
    print("array img：",img_array)
    print("shape of arry img：",img_array.shape)

#realiser algo regionGrow avec img sub back
def regionGrowWithSubBack(img,imgBack ,seeds, p=1):
    #obtient un img apre sub back
    imgSubBack=obtientImgApresSubBack(img, imgBack)


    #charger front
    fg = cv2.imread(img)
    #transfer frond page to hsv
    hsv = cv2.cvtColor(fg, cv2.COLOR_BGR2HSV)

    img = cv2.imread(img)

    #cv2.imshow("apres sub", img)
    # img.shape. It returns a tuple of the number of rows, columns, and channels
    #height : ligne weight : cologne color:len pour list coleur
    height, weight,color = img.shape
    #Return a new array of given shape and type, filled with zeros.
    #ini carte marque pour chaque pixel comme 0
    seedMark = np.zeros(imgSubBack.shape)

    #ini carte affichier pour resultat
    resultat = np.zeros(fg.shape)

    seedList = []
    for seed in seeds:
        # list. append (x). Ajoute un élément à la fin de la liste
        seedList.append(seed)
    label = 1
    #choisir type de connect au 8 dimention
    connects = selectConnects(p)
    #tester si tous les seed au choix a deja utilise
    while (len(seedList) > 0):
        #tenir premier seed qu choix
        currentPoint = seedList.pop(0)
        #marquer ce seed au choix
        seedMark[currentPoint.x, currentPoint.y] = label
        #fiare un boucle avec 8 fois iteration
        for i in range(8):
            #choisir un point connecter ce point au choix
            tmpX = currentPoint.x + connects[i].x
            tmpY = currentPoint.y + connects[i].y
            #si ordre de graphe on quitter cette fois
            if tmpX < 0 or tmpY < 0 or tmpX >= height or tmpY >= weight:
                continue

            #tester si ce pixel est correspend dans contour homme avec imgSubBack
            #on le marque et ajouter dans seed list
            if imgSubBack[tmpX, tmpY] == 255 and seedMark[tmpX, tmpY] == 0:
                seedMark[tmpX, tmpY] = label
                seedList.append(Point(tmpX, tmpY))
                #detecter bleu on donne rouge
                if tsetTypeColor(hsv[tmpX, tmpY])==1:
                    resultat[tmpX, tmpY] = np.array([255,0,0])
                #detecter soin on donne vert
                if tsetTypeColor(hsv[tmpX, tmpY])==2:
                    resultat[tmpX, tmpY] = np.array([0,255,0])
                #detecter noir on donne bleu
                if tsetTypeColor(hsv[tmpX, tmpY])==3:
                    resultat[tmpX, tmpY] = np.array([0,0,255])
                #detecter gris on donne coleur jeun
                if tsetTypeColor(hsv[tmpX, tmpY])==4:
                    resultat[tmpX, tmpY] = np.array([0,255,255])

    return resultat
'''
tester type de couleur avec HSV
1 bleu, 2 soin, 3 noir, 4 gris, -1 inconnu
'''
def tsetTypeColor(pixel):
    if(len(pixel)!=3):
        return -1
    if pixel[0] >= 55 and pixel[0] <= 118 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[2] <= 255:
        return 1
    if pixel[0] >= 0 and pixel[0] <= 9 and pixel[1] >= 48 and pixel[1] <= 255 and pixel[2] >= 50 and pixel[2] <= 255:
        return 2
    if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[2] <= 46:
        return 3
    if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 43 and pixel[2] >= 46 and pixel[2] <= 220:
        return 4
    return -1



#使用了背景减法
#utilise facon sub back a la main
def obtientImgApresSubBack(path,pthBack):
    # load the background and foreground images
    # 导入背景，前景文件
    bg = cv2.imread(pthBack)
    fg = cv2.imread(path)

    #Filtrage
    fl=Filtrage('f')
    bg=fl.filtageGauss(pthBack)
    fg=fl.filtageGauss(path)

    # convert the background and foreground images to grayscale
    # 灰度化处理
    bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
    fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
    # perform background subtraction by subtracting the foreground from
    # the background and then taking the absolute value
    # 背景减法
    sub = bgGray.astype("int32") - fgGray.astype("int32")
    sub = np.absolute(sub).astype("uint8")
    # threshold the image to find regions of the subtracted image with
    # larger pixel differences
    thresh = cv2.threshold(sub, 0, 255,
                           cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    #monterer img apres traite
    #cv2.imshow("subBack", thresh)
    #cv2.waitKey(0)

    # dialotion img
    thresh = dialotion(thresh)
    return thresh

#realser image suback mais garder les couleur
def showImgAfterSubBack(img, imgBack):
    # obtient un img apre sub back
    imgSubBack = obtientImgApresSubBack(img, imgBack)
    img = cv2.imread(img)
    #trnsfer img
    imgRGB = cv2.cvtColor(imgSubBack, cv2.COLOR_GRAY2RGB)
    img_mask = imgRGB
    sub_img = cv2.bitwise_and(img, img_mask)
    cv2.imshow("subBack", sub_img)
    #cv2.waitKey(0)
    return sub_img

# dialotion the img
def dialotion( img):
    # 创建 核 creer coeur
    kernel = np.ones((5, 5), np.uint8)
    # 膨胀 nombre iteration=2
    dilate_img = cv2.dilate(img, kernel, 2)
    return dilate_img


#tester
img = cv2.imread('media/images/homme2.png', 0)
back = cv2.imread('media/images/back.png', 0)
pathName = 'media/images/homme2.png'
pathBackName = 'media/images/back.png'

showImgAfterSubBack(pathName,pathBackName)
seeds = [Point(0, 0), Point(300, 500), Point(20, 300)]
binaryImg =regionGrowWithSubBack(pathName, pathBackName,seeds)
printEnArray(binaryImg)
cv2.imshow('show after region growing', binaryImg)
cv2.waitKey(0)



