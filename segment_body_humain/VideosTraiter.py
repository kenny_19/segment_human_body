import imutils as imutils
import numpy as np
import cv2
import argparse

from matplotlib import pyplot as plt


class VideosTraiter:
    def __init__(self, name):
        self.name = name

    #BackgroundSubtractorMOG2
    def traiteAvecMOG2Video(self,path):
        #a VideoCapture object is used to read the input video
        cap = cv2.VideoCapture(path)
        # create Background Subtractor objects
        fgbg = cv2.createBackgroundSubtractorMOG2()
        while(1):
            ret, frame = cap.read()
            # update the background model
            fgmask = fgbg.apply(frame)
            # show the current frame and the fg masks
            cv2.imshow('frame',fgmask)
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break
        cap.release()
        cv2.destroyAllWindows()

    #BackgroundSubtractor avec images
    #il faut meme taille de img
    def traiteAvecMOG2Img(self,path,pthBack):
        # load the background and foreground images
        # 导入背景，前景文件
        bg = cv2.imread(pthBack)
        fg = cv2.imread(path)
        # convert the background and foreground images to grayscale
        # 灰度化处理
        bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
        # perform background subtraction by subtracting the foreground from
        # the background and then taking the absolute value
        # 背景减法
        sub = bgGray.astype("int32") - fgGray.astype("int32")
        sub = np.absolute(sub).astype("uint8")
        cv2.imshow("sub", sub)
        # threshold the image to find regions of the subtracted image with
        # larger pixel differences
        thresh = cv2.threshold(sub, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        cv2.imshow("thresh", thresh)

        # perform a series of erosions and dilations to remove noise
        # erode ,dilate 降噪处理
        thresh = cv2.erode(thresh, None, iterations=1)
        thresh = cv2.dilate(thresh, None, iterations=1)
        cv2.imshow("thresh2", thresh)

        # find contours in the thresholded difference map and then initialize
        # 发现边界
        # our bounding box regions that contains the *entire* region of motion
        cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        # 给边界初始值
        (minX, minY) = (np.inf, np.inf)
        (maxX, maxY) = (-np.inf, -np.inf)

        # loop over the contours
        # 循环计算边界
        for c in cnts:
            # compute the bounding box of the contour
            (x, y, w, h) = cv2.boundingRect(c)

            # reduce noise by enforcing requirements on the bounding box size
            # 如果边界值，w 或 w 小于20 就认为是噪音
            if w > 20 and h > 20:
                # update our bookkeeping variables
                minX = min(minX, x)
                minY = min(minY, y)
                maxX = max(maxX, x + w - 1)
                maxY = max(maxY, y + h - 1)

        # draw a rectangle surrounding the region of motion
        # 绘制长方形
        cv2.rectangle(fg, (minX, minY), (maxX, maxY), (0, 255, 0), 2)

        # show the output image
        # 输出图形
        cv2.imshow("fg", fg)
        cv2.imshow("bg", bg)
        cv2.waitKey(0)

    def obtientImgApresSubBackOrigion(self,path,pthBack):
        # load the background and foreground images
        # 导入背景，前景文件
        bg = cv2.imread(pthBack)
        fg = cv2.imread(path)
        # convert the background and foreground images to grayscale
        # 灰度化处理
        bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
        # perform background subtraction by subtracting the foreground from
        # the background and then taking the absolute value
        # 背景减法
        sub = bgGray.astype("int32") - fgGray.astype("int32")
        sub = np.absolute(sub).astype("uint8")
        # threshold the image to find regions of the subtracted image with
        # larger pixel differences
        thresh = cv2.threshold(sub, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        #thresh1 = cv2.adaptiveThreshold(sub,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
        #monterer img apres traite
        cv2.imshow("thresh", thresh)
        #cv2.imshow("thresh1", thresh1)
        cv2.waitKey(0)
        return thresh


    def getPerson(self,image,frame,fgbg,opt=1):

        # get the front mask
        mask = fgbg.apply(frame)

        # eliminate the noise
        line = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 5), (-1, -1))
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, line)
        cv2.imshow("mask", mask)

        # find the max area contours
        contours,hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for c in range(len(contours)):
            area = cv2.contourArea(contours[c])
            if area < 150:
                continue
            rect = cv2.minAreaRect(contours[c])
            cv2.ellipse(image, rect, (0, 255, 0), 2, 8)
            cv2.circle(image, (np.int32(rect[0][0]), np.int32(rect[0][1])), 2, (255, 0, 0), 2, 8, 0)
        return image, mask

    def useGetPerson(self,path):
        # read the video
        cap = cv2.VideoCapture(path)

        # create the subtractor
        fgbg = cv2.createBackgroundSubtractorMOG2(
            history=500, varThreshold=100, detectShadows=False)

        while True:
            ret, frame = cap.read()
            cv2.imwrite("media/imges/input.png", frame)
            cv2.imshow('input', frame)
            result, m_ = self.getPerson(frame,frame,fgbg)
            cv2.imshow('result', result)
            k = cv2.waitKey(50)&0xff
            if k == 27:
                cv2.imwrite("media/imges/result.png", result)
                cv2.imwrite("media/imges/mask.png", m_)

                break
        cap.release()
        cv2.destroyAllWindows()





vt = VideosTraiter('trateVideos')
vt.traiteAvecMOG2Video('media/videos/test2.avi')
#vt.obtientImgApresSubBackOrigion('media/images/homme2.png','media/images/back.png')
#vt.traiteAvecMOG2Img('media/images/homme2.png','media/images/back.png')
#vt.useGetPerson('media/videos/test2.avi')
