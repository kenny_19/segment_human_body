import time

import numpy
import cv2
import numpy as np
from Filtrage import Filtrage

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

class SegmentCorpe:
    def __init__(self, name):
        self.name = name

    # sub img avec resultqt en gris
    def obtientImgApresSubBack(self, path, pthBack):
        # load the background and foreground images
        # 导入背景，前景文件
        bg = cv2.imread(pthBack)
        fg = cv2.imread(path)

        # Filtrage
        fl = Filtrage('f')
        bg = fl.filtageGauss(pthBack)
        fg = fl.filtageGauss(path)

        # convert the background and foreground images to grayscale
        # 灰度化处理
        bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
        # perform background subtraction by subtracting the foreground from
        # the background and then taking the absolute value
        # 背景减法
        sub = bgGray.astype("int32") - fgGray.astype("int32")
        sub = np.absolute(sub).astype("uint8")
        # threshold the image to find regions of the subtracted image with
        # larger pixel differences
        thresh = cv2.threshold(sub, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        # monterer img apres traite
        # cv2.imshow("subBack", thresh)
        # cv2.waitKey(0)
        return thresh

    #dialotion the img
    def dialotion(self,img):
        # 创建 核 creer coeur
        kernel = np.ones((5, 5), np.uint8)
        # 膨胀 nombre iteration=2
        dilate_img = cv2.dilate(img, kernel, 2)
        return dilate_img

    #faire segmentation pour ce image
    #normalement on le segment par 9 part
    def segCorp(self,img):
        print("hello")

    #draw le contours avec plus grand longeur avec rectangle bleu
    def chercheContourMax(self, img1):
        #do dialotion
        img1=self.dialotion(img1)
        #cherche tousn les contour
        cnts, hierarchy = cv2.findContours(img1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        #trnsfer du gris vers rgb
        img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
        img = np.copy(img1)


        #on calculer la langeur de contour moynne
        # 4.计算平均周长
        lengths = list()
        for i in range(len(cnts)):
            length = cv2.arcLength(cnts[i], True)
            lengths.append(length)
            #print("contour %d avec longeur: %d" % (i, length))

        length_avg = np.average(lengths)
        #print("longeur contour moyenne:", length_avg)

        img_contours = []

        #choisir plus grand largeur de contour
        largeurMax=0
        indexContour=-1
        for i in range(len(cnts)):
            img_temp = np.zeros(img.shape, np.uint8)
            img_contours.append(img_temp)

            length = cv2.arcLength(cnts[i], True)
            # on prend contour qui a loneurs superieur au langeur moyenne
            if length > length_avg:
                #print("contour %d avec longeur:%d" % (i, length))
                if length > largeurMax:
                    largeurMax=length
                    indexContour = i

        if(indexContour!=-1):
            # draw contours with img
            x, y, w, h = cv2.boundingRect(cnts[indexContour])
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)  # blue
            #(x,y) correspond point gauche haut
            #(x + w, y + h) correspont point droit base
            #cv2.circle(img, (x + w, y + h), 10, (255,255,0), 0)
            return img, (x, y), (x + w, y + h)
        else:
            return img, (0, 0), (0, 0)

    # 使用了背景减法
    # utilise facon sub back a la main
    def obtientImgApresSubBack(self,path, pthBack):
        # load the background and foreground images
        # 导入背景，前景文件
        bg = cv2.imread(pthBack)
        fg = cv2.imread(path)

        # Filtrage
        fl = Filtrage('f')
        bg = fl.filtageGauss(pthBack)
        fg = fl.filtageGauss(path)

        # convert the background and foreground images to grayscale
        # 灰度化处理
        bgGray = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
        fgGray = cv2.cvtColor(fg, cv2.COLOR_BGR2GRAY)
        # perform background subtraction by subtracting the foreground from
        # the background and then taking the absolute value
        # 背景减法
        sub = bgGray.astype("int32") - fgGray.astype("int32")
        sub = np.absolute(sub).astype("uint8")
        # threshold the image to find regions of the subtracted image with
        # larger pixel differences
        thresh = cv2.threshold(sub, 0, 255,
                               cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        # monterer img apres traite
        # cv2.imshow("subBack", thresh)
        # cv2.waitKey(0)

        # dialotion img
        thresh = self.dialotion(thresh)
        return thresh

    # realser image suback mais garder les couleur
    def showImgAfterSubBack(self,img, imgBack):
        # obtient un img apre sub back
        imgSubBack = self.obtientImgApresSubBack(img, imgBack)
        img = cv2.imread(img)
        # trnsfer img
        imgRGB = cv2.cvtColor(imgSubBack, cv2.COLOR_GRAY2RGB)
        img_mask = imgRGB
        sub_img = cv2.bitwise_and(img, img_mask)
        #cv2.imshow("subBack", sub_img)
        # cv2.waitKey(0)
        return sub_img

    # segment image avec couleur
    #avec une couleurs indique
    def SegColor(self, pathF,coleurs):
        # tester si path ou img
        if type(pathF) == type("a"):
            img = cv2.imread(pathF)
        else:
            img = pathF
        # 双边滤波器，同时考虑像素的空间位置和邻域的像素灰度相似性，在滤波的同时较好的保留图片的边缘
        blur4 = cv2.bilateralFilter(img, 9, 75, 75)
        # 将图像与RGB颜色空间转到HSV颜色空间
        # HSV空间，H、S和V相对独立，可以准确描述像素的色度，饱和度和亮度
        hsv = cv2.cvtColor(blur4, cv2.COLOR_BGR2HSV)
        # 确定像素的颜色阈值
        # 利用颜色选择工具，确定需要提取的对象的像素值，这是一个繁琐的过程，需要不断尝试，以获取准确的颜色阈值区间。
        # 颜色阈值分割
        # choisir un couleur pour segment
        # couleur de masque
        low_blue = numpy.array([55, 0, 0])
        high_blue = numpy.array([118, 255, 255])
        # couluer de soin
        low_skin = numpy.array([0, 48, 50])
        high_skin = numpy.array([9, 255, 255])
        # couleur de cheveux et mantaux
        low_black = numpy.array([0, 0, 0])
        high_black = numpy.array([180, 255, 46])
        # couleur de pantalon et mantaux
        low_grey = numpy.array([0, 0, 46])
        high_grey = numpy.array([180, 43, 220])
        # couleur brun
        low_brown = numpy.array([10, 100, 20])
        hight_brown = numpy.array([20, 255, 200])

        low_val = low_skin
        high_val = high_skin

        if(coleurs=="bleu"):
            low_val = low_blue
            high_val = high_blue

        mask = cv2.inRange(hsv, low_val, high_val)
        # cv2.imshow('mask', mask)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # 得到最终结果
        # 利用mask将图像中所有不在描述范围的像素覆盖，得到需要的对象。
        res = cv2.bitwise_and(img, img, mask=mask)
        # cv2.imshow('res', res)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        return res

    #find contours et support en image couleur
    def findContourEnCouleur(self,img):
        #transfer img en gris et binaire
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        #ret, binary = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
        #cherche contour
        contours, hierarchy = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # print("longeur contour moyenne:", length_avg)
        img_contours = []
        # choisir plus grand largeur de contour
        largeurMax = 0
        indexContour = -1
        for i in range(len(contours)):
            img_temp = np.zeros(img.shape, np.uint8)
            img_contours.append(img_temp)
            length = cv2.arcLength(contours[i], True)
            # on prend contour qui a loneurs superieur au langeur moyenne
            # print("contour %d avec longeur:%d" % (i, length))
            if length > largeurMax:
                largeurMax = length
                indexContour = i
        if (indexContour != -1):
            # draw contours with img
            x, y, w, h = cv2.boundingRect(contours[indexContour])
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)  # blue
            #print(contour_decroissant)
            return img, (x, y), (x + w, y + h)
        else:
            return img, (0, 0), (0 , 0)


    '''
    realiser algo regionGrow avec img sub back
    p==1 selection connection 8
    partiecorp 0(bras) 1(tete) 2(poitein) 3(jambom)
    '''
    def regionGrowWithSubBack(self, img, imgBack,pointHautGauche,pointBaseDroit,partiecorp=0, p=1):
        #obtient les corredonne
        px1 = pointHautGauche.getX()
        py1 = pointHautGauche.getY()

        px2 = pointBaseDroit.getX()
        py2 = pointBaseDroit.getY()

        #img2 = self.obtientImgApresSubBack(img, imgBack)
        #cv2.rectangle(img2, (px1, py1), (px2, py2), (255, 0, 0), 2)
        #genere seed pour 4*4
        longEnLng = int((py2-py1)/10)
        longEnCol = int((px2-px1)/10)
        nouvListSeed = []
        for i in range(9):
            for j in range(9):
                nouvListSeed.append(Point(py1+j*longEnLng,px1+i*longEnCol))
                #print(y1+j*longEnCol)
                #print(x1 + i * longEnLng)
                #cv2.circle(img2, (py1+j*longEnLng,px1+i*longEnCol), 1, (0,0,255), 4)

        #cv2.imshow('region', img2)

        # obtient un img apre sub back
        imgSubBack = self.obtientImgApresSubBack(img, imgBack)

        # charger front
        fg = cv2.imread(img)
        # transfer frond page to hsv
        #hsv = cv2.cvtColor(fg, cv2.COLOR_BGR2HSV)

        img = cv2.imread(img)

        # cv2.imshow("apres sub", img)
        # img.shape. It returns a tuple of the number of rows, columns, and channels
        # height : ligne weight : cologne color:len pour list coleur
        height, weight, color = img.shape
        # Return a new array of given shape and type, filled with zeros.
        # ini carte marque pour chaque pixel comme 0
        seedMark = np.zeros(imgSubBack.shape)
        #print(imgSubBack.shape)

        # ini carte affichier pour resultat
        resultat = np.zeros(fg.shape)

        seedList = nouvListSeed
        #for seed in seeds:
            # list. append (x). Ajoute un élément à la fin de la liste
            #seedList.append(seed)
        label = 1
        # choisir type de connect au 8 dimention
        connects = self.selectConnects(p)
        # tester si tous les seed au choix a deja utilise
        while (len(seedList) > 0):
            # tenir premier seed qu choix
            currentPoint = seedList.pop(0)
            # marquer ce seed au choix
            seedMark[currentPoint.x, currentPoint.y] = label
            # fiare un boucle avec 8 fois iteration
            for i in range(8):
                # choisir un point connecter ce point au choix
                lng = currentPoint.x + connects[i].x
                col = currentPoint.y + connects[i].y
                # si ordre de graphe on quitter cette fois
                if lng < 0 or col < 0 or lng >= height or col >= weight:
                    continue
                '''
                 tester si ce pixel est correspend dans contour homme avec imgSubBack
                 on le marque et ajouter dans seed list
                 tester type de region d'homme 0 pour tous
                 tester si dans la region
                 attenstion la tempX correspond en col
                '''
                if imgSubBack[ lng,col] == 255 and seedMark[lng,col] == 0 \
                        and lng<py2 and lng>py1\
                        and col<px2 and col>px1:
                    #print("trouver")
                    seedMark[lng, col] = label
                    seedList.append(Point(lng, col))
                    # detecter bleu on donne rouge
                    #if self.tsetTypeColor(hsv[tmpX, tmpY]) == 1:
                    #    resultat[tmpX, tmpY] = np.array([255, 0, 0])
                    if(partiecorp==0):
                        resultat[lng, col] = np.array([255, 0, 0])
                    if (partiecorp == 1):
                        resultat[lng, col] = np.array([ 0,255, 0])
                    if (partiecorp == 2):
                        resultat[lng, col] = np.array([ 0, 0,255])
                    if (partiecorp == 3):
                        resultat[lng, col] = np.array([ 0,255, 255])

        return resultat

    '''
    tester type de couleur avec HSV
    1 bleu, 2 soin, 3 noir, 4 gris, -1 inconnu
    '''

    def tsetTypeColor(self, pixel):
        if (len(pixel) != 3):
            return -1
        if pixel[0] >= 55 and pixel[0] <= 118 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[
            2] <= 255:
            return 1
        if pixel[0] >= 0 and pixel[0] <= 9 and pixel[1] >= 48 and pixel[1] <= 255 and pixel[2] >= 50 and pixel[
            2] <= 255:
            return 2
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 255 and pixel[2] >= 0 and pixel[
            2] <= 46:
            return 3
        if pixel[0] >= 0 and pixel[0] <= 180 and pixel[1] >= 0 and pixel[1] <= 43 and pixel[2] >= 46 and pixel[
            2] <= 220:
            return 4
        return -1

    # creer un liste avec 8 point ou 4 point qui montrer les dimentions
    def selectConnects(self, p):
        if p != 0:
            connects = [Point(-1, -1), Point(0, -1), Point(1, -1), Point(1, 0), Point(1, 1),
                        Point(0, 1), Point(-1, 1), Point(-1, 0)]
        else:
            connects = [Point(0, -1), Point(1, 0), Point(0, 1), Point(-1, 0)]
        return connects

    # print img en array
    def printEnArray(self, img):
        img_array = np.asarray(img)
        print("array img：", img_array)
        print("shape of arry img：", img_array.shape)

    # segment un homme basic
    def segmentBasic(self,pathName,pathBackName):
        frame = sc.obtientImgApresSubBack(pathName, pathBackName)

        time_start = time.time()
        d2 = self.showImgAfterSubBack(pathName, pathBackName)

        # montrer le contour de ce personne
        d, (x1, y1), (x2, y2) = self.chercheContourMax(frame)

        d5 = d2
        # segement personne en coleur
        # on se trouve la tete avec masque
        resSegCol = self.SegColor(d2, "bleu")
        d3, (x5, y5), (x6, y6) = self.findContourEnCouleur(resSegCol)

        #on se trouve pentalong
        resSegCol2 = self.SegColor(d2, "skin")
        d4, (x3, y3), (x4, y4) = self.findContourEnCouleur(resSegCol2)

        # draw lingne pour segmentation
        cv2.line(d5, pt1=(x1, y6), pt2=(x2, y6), color=(0, 255, 0), thickness=5)
        cv2.line(d5, pt1=(x1, y3), pt2=(x2, y3), color=(0, 255, 0), thickness=5)
        cv2.line(d5, pt1=(x3, y1), pt2=(x3, y2), color=(0, 255, 0), thickness=5)
        cv2.line(d5, pt1=(x4, y1), pt2=(x4, y2), color=(0, 255, 0), thickness=5)


        # gene bras
        binaryImg = self.regionGrowWithSubBack(pathName, pathBackName, Point(x1, y1), Point(x3, y2))
        binaryImg2 = self.regionGrowWithSubBack(pathName, pathBackName, Point(x4, y1), Point(x2, y3))

        # gene tete
        binaryImg3 = self.regionGrowWithSubBack(pathName, pathBackName, Point(x3, y1), Point(x4, y6), 1)

        # gene poitein
        binaryImg4 = self.regionGrowWithSubBack(pathName, pathBackName, Point(x3, y6), Point(x4, y3), 2)

        # gene jambon
        binaryImg5 = self.regionGrowWithSubBack(pathName, pathBackName, Point(x3, y3), Point(x4, y2), 3)

        # fusion les resutat
        img_mix = cv2.addWeighted(binaryImg, 1, binaryImg2, 1, 0)
        img_mix = cv2.addWeighted(img_mix, 1, binaryImg3, 1, 0)
        img_mix = cv2.addWeighted(img_mix, 1, binaryImg4, 1, 0)
        img_mix = cv2.addWeighted(img_mix, 1, binaryImg5, 1, 0)
        time_end = time.time()

        print(' temp passe ',time_end-time_start,'s')

        # affichier les chose que on interesse
        cv2.imshow('input', frame)
        cv2.imshow('img printe lingne', d5)
        cv2.imshow('chercher contour', d)
        cv2.imshow("subBack En couleur skin et cherche contour", d4)
        cv2.imshow("subBack En couleur bleu et cherche contour", d3)
        cv2.imshow('img apres sub back', d2)
        cv2.imshow('bras 1', binaryImg)
        cv2.imshow('bras 2', binaryImg2)
        cv2.imshow('tete', binaryImg3)
        cv2.imshow('poitein', binaryImg4)
        cv2.imshow('jambon', binaryImg5)
        cv2.imshow('homme complete', img_mix)

if __name__ == '__main__':
    pathName = 'media/images/homme1.png'
    pathBackName = 'media/images/back.png'
    sc = SegmentCorpe('segCorp')
    #frame = sc.obtientImgApresSubBack(pathName, pathBackName)
    # segment un homme basic
    sc.segmentBasic(pathName, pathBackName)

    cv2.waitKey(0)

