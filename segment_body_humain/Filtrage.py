import cv2
import numpy as np
class Filtrage:
    def __init__(self, name):
        self.name = name

    #filtrage avec Convolution matrice 2D
    def filtrage2DMatrice(self, path):
        o=cv2.imread(path)
        kernel=np.ones((9,9),np.float32)/81
        r=cv2.filter2D(o,-1,kernel)
        return r;
        #cv2.imshow('origin',o)
        #cv2.imshow('result',r)
        #cv2.waitKey()
        #cv2.destroyAllWindows()

    #filtrage avec Convolution Box comme FiltrageConvolution
    def filtrageConvolutionBox(self,path):
        o = cv2.imread(path)
        r = cv2.boxFilter(o,-1,(2,2),normalize=0)
        return r
        '''
        cv2.imshow('origin', o)
        cv2.imshow('result', r)
        cv2.waitKey()
        cv2.destroyAllWindows()
        '''


    #filtrage avec Convolution bidirection comme FiltrageDetectionContour
    def filtrageBidrection(self,path):
        o = cv2.imread(path)
        r=cv2.GaussianBlur(o,(55,55),0,0)
        b=cv2.bilateralFilter(o,55,100,100)
        return b
        '''
        cv2.imshow('origin', o)
        cv2.imshow('gaussian', r)
        cv2.imshow('bilateral', b)
        cv2.waitKey()
        cv2.destroyAllWindows()
        '''


    #filtrage en moyenne
    def filtrageEnMoyenne(self,path):
        o = cv2.imread(path)
        r =cv2.blur(o,(5,5))
        return r
        '''
        cv2.imshow('origin', o)
        cv2.imshow('result', r)
        cv2.waitKey()
        cv2.destroyAllWindows()
        '''


    #filtrage en gaussien
    def filtageGauss(self,path):
        o = cv2.imread(path)
        r = cv2.GaussianBlur(o, (5, 5), 0, 0)
        return r
        '''
        cv2.imshow('origin', o)
        cv2.imshow('result', r)
        cv2.waitKey()
        cv2.destroyAllWindows()
        '''

